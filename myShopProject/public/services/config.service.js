(function() {
    'use strict';

    angular
        .module('myShop')
        .factory('ConfigService', [ConfigService]);

        function ConfigService() {
            var Config = {};

            Config.apiUrl = 'http://localhost:3030';

            return Config;
        }
}());
