(function(){
    'use strict';

    angular
        .module('myShop')
        .config(routes)

        function routes($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state({
                    name: 'main',
                    url: '/main',
                    controller: 'MainController',
                    controllerAs: 'main',
                    templateUrl: 'components/main/templates/main.template.html'
                })
                .state({
                    name: 'main.products',
                    url: '/products',
                    controller: 'ProductsController',
                    controllerAs: 'products',
                    templateUrl: 'components/products/templates/products.template.html'
                })
                .state({
                    name: 'main.products.manufacturer',
                    url: '/by/:name',
                    templateUrl: 'components/products/templates/products.by.manufacturer.template.html'
                })
                .state({
                    name: 'main.product',
                    url: '/product',
                    controller: 'ProductController',
                    controllerAs: 'product',
                    templateUrl: 'components/products/templates/product.template.html'
                })
                .state({
                    name: 'main.product.detail',
                    url: '/detail/:id',
                    templateUrl: 'components/products/templates/detail.template.html'
                })
                .state({
                    name: 'main.product.edit',
                    url: '/edit/:id',
                    templateUrl: 'components/products/templates/form.template.html'
                })
                .state({
                    name: 'main.product.new',
                    url: '/new',
                    templateUrl: 'components/products/templates/form.template.html'
                })
                .state({
                    name: 'main.manufacturers',
                    url: '/manufacturers',
                    controller: 'ManufacturersController',
                    controllerAs: 'manufacturers',
                    templateUrl: 'components/manufacturers/templates/manufacturers.template.html'
                })
                .state({
                    name: 'main.manufacturer',
                    url: '/manufacturer',
                    controller: 'ManufacturerController',
                    controllerAs: 'manufacturer',
                    templateUrl: 'components/manufacturers/templates/manufacturer.template.html'
                })
                .state({
                    name: 'main.manufacturer.detail',
                    url: '/detail/:id',
                    templateUrl: 'components/manufacturers/templates/detail.template.html'
                })
                .state({
                    name: 'main.manufacturer.edit',
                    url: '/edit/:id',
                    templateUrl: 'components/manufacturers/templates/form.template.html'
                })
                .state({
                    name: 'main.manufacturer.new',
                    url: '/new',
                    templateUrl: 'components/manufacturers/templates/form.template.html'
                });

            $urlRouterProvider.otherwise('/main');
        };
}());
