(function(){
    'use strict';

    angular
        .module('myShop')
        .controller('ManufacturersController', ['$state', '$http', 'Api', ManufacturersController]);

        function ManufacturersController ($state, $http, Api) {

            var self = this;

            self.manufacturers = [];

            console.log($state.current.name);

            // Get products list function
            // return: array with all products

            Api.get('/producators')
                .then(function success (res) {
                    self.manufacturers = res.data;
                    console.log(res);
                }, function error (err) {
                    console.log('callback error', err);
                });

            // Delete a product
            self.deleteManufacturer = function deleteContact(id, index) {
                Api.delete('/producators', id, index)
                    .then(function success (res) {
                        self.manufacturers.splice(index, 1);
                        console.log(res);
                    }, function error (err) {
                        console.log('callback error', err);
                    });
            };

            // Passing id to stateParams
            self.getManufacturer = function getContact(id, name) {
                $state.go('main.manufacturer.detail', {
                    id: id,
                    name: name
                });
            };

            // Passing id to stateParams edit
            self.editManufacturer = function editManufacturer (id) {
                $state.go('main.manufacturer.edit', {
                    id: id,
                    name: name
                });
            };


            // self.productByManufacturer = function productByManufacturer (name) {
            //     $state.go('main.products.manufacturer', {
            //         name: name
            //     });
            // };

        }

}());
