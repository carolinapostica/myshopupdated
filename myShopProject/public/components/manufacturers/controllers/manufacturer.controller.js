(function() {
    'use strict';

    angular
        .module('myShop')
        .controller('ManufacturerController', ['$state', '$stateParams',  '$http', 'Api', ManufacturerController]);

    function ManufacturerController ($state, $stateParams, $http, Api) {

        var self = this;
        self.manufacturer = {};

        console.log($state.current.name);

        // Get a product by Id
        self.getManufacturerById = function getmanufacturerById() {
            var url = '/producators/' + $state.params.id;
            Api.get(url)
            .then(function success(res) {
                self.manufacturer = res.data;
                console.log(self.manufacturer);
            }, function error(err) {
                console.log(err);
            });
        };

        if ($state.current.name !== 'main.manufacturer.new') {
            self.getManufacturerById();
        }

        // Create a new product
        self.addNewManufacturer = function addNewManufacturer() {
            Api.post('/producators', self.manufacturer)
                .then(function success(res) {
                    self.manufacturers = res.data;
                    console.log('1:' + self.manufacturers);
                }, function error(err) {
                    console.log(err);
                });
        };

        // Edit product
        self.editDetailManufacturer = function editDetailManufacturer (id) {
            $state.go('main.manufacturer.edit', {
                id: id
            });
            Api.put('/producators', $state.params.id, self.manufacturer)
                .then(function success(res) {
                    self.manufacturer = res.data;
                    console.log(res);

                }, function error(err) {
                    console.log(err);
                });
        };

        // Funtions that determine the page on which user is currently on:
        // edit product page
        self.isEditMode = function isEditMode() {
            return $state.current.name === 'main.manufacturer.edit';

        };
        // add new product page
        self.isAddMode = function isAddMode() {
            return $state.current.name === 'main.manufacturer.new';
        };

        // Submit funciton for adding new product and editing existing ones
        self.saveManufacturer = function saveManufacturer (id) {
            console.log(self.isAddMode());
            if (self.isAddMode()) {
                self.addNewManufacturer ();
                $state.go('main.manufacturers');
            } else {
                self.editDetailManufacturer ();
                $state.go('main.manufacturers');
            }
        };

        //Products by manufacturer
        self.productsByManufacturers = function productsByManufacturers(name) {
            $state.go('main.products.manufacturer', {
                name: name
            });
            console.log( $state.params);
            var url = '/products/by/' + $state.params.name;
            Api.get(url)
            .then(function success(res) {
                self.manufacturer = res.data;
                console.log(self.manufacturer);
            }, function error(err) {
                console.log(err);
            });
        }
    }

}());
