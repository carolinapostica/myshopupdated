(function(){
    'use strict';

    angular
        .module('myShop')
        .controller('MainController', ['$state', MainController]);

        function MainController($state) {

            var self = this;

            console.log($state.current.name);
            self.isMain = function isMain () {
            	return $state.current.name === 'main';
            };
        }

}());
