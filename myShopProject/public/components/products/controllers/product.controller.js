(function() {
    'use strict';

    angular
        .module('myShop')
        .controller('ProductController', ['$state', '$stateParams',  '$http', 'Api', ProductController]);

    function ProductController($state, $stateParams, $http, Api) {

        var self = this;
        self.product = {};

        console.log($state.current.name);

        // Get a product by Id
        self.getProductById = function getProductById() {
            var url = '/products/' + $state.params.id;
            Api.get(url)
            .then(function success(res) {
                self.product = res.data;
                console.log(self.product);
            }, function error(err) {
                console.log(err);
            });
        };

        if ($state.current.name !== 'main.product.new') {
            self.getProductById();
        }

        // Create a new product
        self.addNewProduct = function addNewProduct() {
            Api.post('/products/', self.product)
                .then(function success(res) {
                    self.productList = res.data;

                }, function error(err) {
                    console.log(err);
                });
        };

        // Edit product
        self.editDetailProduct = function editDetailProduct(id) {
            $state.go('main.product.edit', {
                id: id
            });

            Api.put('/products', $state.params.id, self.product)
                .then(function success(res) {
                    self.product = res.data;
                    console.log(res);

                }, function error(err) {
                    console.log(err);
                });
        };

        // Funtions that determine the page on which user is currently on:
        // edit product page
        self.isEditMode = function isEditMode() {
            return $state.current.name === 'main.product.edit';

        };
        // add new product page
        self.isAddMode = function isAddMode() {
            return $state.current.name === 'main.product.new';
        };

        // Submit funciton for adding new product and editing existing ones
        self.saveProduct = function saveProduct(id) {
            console.log($state.current.name);
            if (self.isAddMode()) {
                self.addNewProduct();
                $state.go('main.products');
            } else {
                self.editDetailProduct();
                $state.go('main.products');
            }
        };
    }

}());
