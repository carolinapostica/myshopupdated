(function(){
    'use strict';

    angular
        .module('myShop')
        .controller('ProductsController', ['$state', '$http', 'Api', ProductsController]);

        function ProductsController($state, $http, Api) {

            var self = this;

            self.products = [];

            console.log($state.current.name);

            // Get products list function
            // return: array with all products

            Api.get('/products')
                .then(function success (res) {
                    self.productList = res.data;
                    console.log(res);
                }, function error (err) {
                    console.log('callback error', err);
                });

            // Delete a product
            self.deleteProduct = function deleteProduct(id, index) {
                Api.delete('/products', id, index)
                    .then(function success (res) {
                        self.productList.splice(index, 1);
                        console.log(res);
                    }, function error (err) {
                        console.log('callback error', err);
                    });
            };

            // Passing id to stateParams
            self.getProduct = function getProduct(id) {
                $state.go('main.product.detail', {
                    id: id
                });
            };

            // Passing id to stateParams edit
            self.editProduct = function editProduct(id) {
                $state.go('main.product.edit', {
                    id: id
                });
            };

            


        }

}());
