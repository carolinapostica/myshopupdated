//Uuid for unique id of product//
var Uuid = require('node-uuid');

//Arrau of all products
var dataArray = [
        {
            id : "1",
            name : "SomeNameProduct",
            type : "SomeTypeName",
            ttn : "234234234234",
            description : "WTF TOVAR",
            producator : "Producator1"
        },
        {
            id : "123",
            name : "SomeNameProduct",
            type : "SomeTypeName",
            ttn : "12312312312",
            description : "Cooles product Ever",
            producator : "Producator2"
        },
        {
            id : "123",
            name : "SomeNameProduct",
            type : "SomeTypeName",
            ttn : "12312312312",
            description : "Worst product Ever",
            producator : "Producator1"
        }
];
////////////////////////////////////////

/////////////////////////////////////////
// FUNCTIONS for EXPORTS
////////////////////////////////////////

//get all Products
exports.getAllProducts = function getAllProducts (req , res) {
    /*
    req : nothing;
    res : array of obj [{},{},....{}];
    */
    console.log('get all products');
    res.send(dataArray);
};

//get product by Id
exports.getProductById = function getProductById (req , res) {
    /*
     req : params.id
     res : {
            name : "blbablbla",
            type : "blalblba",
            ttn : "number blalalala",
            discription : "lalalala blalablab VODI4ka"
           }
    */
    console.log('get product by id');

    var flag = false; //flag to verify if exists product by params.id
    var sendObj = {}; //object , wich  we will send

    dataArray.forEach(function (el) {

        if (el.id == req.params.id) {
            sendObj = el;
            flag = true;
        }

    });

    if (flag) { // if element with this id exists
        res.send(sendObj);
    } else {// else send error msg with status code 404
        res.status(404).send('error with getting by Id');
    }

};

//add new product
exports.addProduct = function addProduct (req ,res) {
    /*
    req : body.name , body.type , body.ttn , body.description
    res : 'ok' if addig was succesful and err msg if was catching an error
    TODO
    VALIDATION OF INPUT DATA WILL BE done on client
     */
    console.log(req.body);
    try {
        console.log(req.body);
        var dataAdd  = {};

        dataAdd.id = 'product_id_' + Uuid.v4();
        dataAdd.name = req.body.name;
        dataAdd.type = req.body.type;
        dataAdd.ttn =  req.body.ttn;
        dataAdd.producator = req.body.producator;
        dataAdd.description = req.body.description;

        dataArray.push(dataAdd);
        console.log(dataAdd);
        res.send('ok');
    } catch (err) {

      console.log('An Error was catching ' , err);
      res.status(404).send('error with adding new product');

    }
};

//delete product by id
exports.deleteById = function deleteById(req , res) {
    /*
    req : params.id
    res : 'ok' if delete was succes and err msg , if was delete product
    */

    var flag = false;
    console.log('delete by id');

    dataArray.forEach(function (el , index) {

        if (el.id === req.params.id) {
            flag = true;
            dataArray.splice(index , 1);
        }

    });

    if (flag) {
        res.send('ok');
    } else {
        res.status(404).send('error with deleting product');
    }
};

//update data by id
exports.updateById = function updateById (req, res) {
    /*
    req : body.id , body.newName , body.newType , body.newTtn , body.newDescription
    res : 'ok' , if all was update succesfuly , err msg if was catch an error
    */

    console.log('update by Id');
    var flag = false;

    dataArray.forEach(function (el , index) {
        if (el.id === req.params.id) {

            el.name = req.body.name;
            el.type = req.body.type;
            el.ttn = req.body.ttn;
            el.producator = req.body.producator;
            el.description = req.body.description;

            flag = true;
            res.send(el);
        }
    });

    if (flag) {
        res.send('ok');
    } else {
        res.status(404).send('error with updating product');
    }
};

//get products by product
exports.productsByProducator = function productsByProducator (req ,res) {
    /*
     req : params.id
     res : [{
            name : "blbablbla",
            type : "blalblba",
            ttn : "number blalalala",
            discription : "lalalala blalablab VODI4ka"
            },...
           ]
    */
    console.log('get product by manufcturer');

    var flag = false; //flag to verify if exists product by params.id
    var sendObj = {}; //object , wich  we will send
    var resdata = [];
    dataArray.forEach(function (el) {

        if (el.producator == req.params.name) {
            resdata.push(el);
            flag = true;
        }
    });

    if (flag) { // if element with this id exists
        res.send(resdata);
    } else {// else send error msg with status code 404
        res.status(404).send('error with getting by Id');
    }
};
