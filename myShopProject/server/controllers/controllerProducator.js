//Uuid for unique id of product//
var Uuid = require('node-uuid');

var dataArrayProducator = [
        {
            id : "123",
            name : "Producator1"
        },
        {
            id : "231",
            name : "Producator2"
        },
        {
            id : "123",
            name : "Producator3"
        },
        {
            id : "312",
            name : "Producator4"
        }
];

//get all producators
exports.getAllProducators = function getAllProducators (req ,res) {
    /*
    req : nothing;
    res : array of obj [{},{},....{}];
    */
    console.log('get all producators');
    res.send(dataArrayProducator);
};

//get producator by id
exports.getProducatorById = function getProducatorById (req ,res) {
    /*
     req : params.id
     res : {
            name : "blbablbla"
           }
    */
    console.log('get product by id');

    var flag = false; //flag to verify if exists product by params.id
    var sendObj = {}; //object , wich  we will send

    dataArrayProducator.forEach(function (el) {

        if (el.id == req.params.id) {
            sendObj = el;
            flag = true;
        }

    });

    if (flag) { // if element with this id exists
        res.send(sendObj);
    } else {// else send error msg with status code 404
        res.status(404).send('error with getting by Id');
    }
};

//add new producator
exports.addProducator = function addProduct (req ,res) {
    /*
    req : body.name
    res : 'ok' if addig was succesful and err msg if was catching an error
    TODO
    VALIDATION OF INPUT DATA WILL BE done on client
     */
    console.log(req.body);
    try {
        console.log(req.body);
        var dataAdd  = {};

        dataAdd.id = 'producator_id_' + Uuid.v4();
        dataAdd.name = req.body.name;

        dataArrayProducator.push(dataAdd);
        console.log(dataAdd);
        res.send(dataAdd);
    } catch (err) {

      console.log('An Error was catching ' , err);
      res.status(404).send('error with adding new product');

    }
};

//delete an producator
exports.deleteProducator = function deleteProducator (req ,res) {
    /*
    req : params.id
    res : 'ok' if delete was succes and err msg , if was delete producator
    */

    var flag = false;
    console.log('delete by id');

    dataArrayProducator.forEach(function (el , index) {

        if (el.id === req.params.id) {
            flag = true;
            dataArrayProducator.splice(index , 1);
        }

    });

    if (flag) {
        res.send('ok');
    } else {
        res.status(404).send('error with deleting producator');
    }
};

//update producator
exports.updateProducator = function updateProducator (req , res) {
    /*
    req : body.id , body.newName
    res : 'ok' , if all was update succesfuly , err msg if was catch an error
    */

    console.log('update by Id');
    var flag = false;

    dataArrayProducator.forEach(function (el , index) {
        if (el.id === req.body.id) {
            el.name = req.body.name;
            flag = true;
            res.send(el);

        }
    });

    if (flag) {
        res.send('ok');
    } else {
        res.status(404).send('error with updating product');
    }
};
