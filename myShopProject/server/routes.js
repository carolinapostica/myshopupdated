// discribe all routes
exports.routes = function routes (app) {


    /*ROUTES FOR PRODUCT*/


    var productsCntrl = require('./controllers/controllersProduct.js');
    //get all products of market
    app.get('/products' , productsCntrl.getAllProducts);
    //get product by id
    app.get('/products/:id' , productsCntrl.getProductById);
    //add new product in Array
    app.post('/products' , productsCntrl.addProduct);
    //delete product by id
    app.delete('/products/:id' , productsCntrl.deleteById);
    //update product by id
    app.put('/products/:id' , productsCntrl.updateById);


    /*ROUTES FOR PRODUCATOR*/


    var producatorCntrl = require('./controllers/controllerProducator.js');
    //get all producators
    app.get('/producators' , producatorCntrl.getAllProducators);
    //get producator by id
    app.get('/producators/:id' , producatorCntrl.getProducatorById);
    //add new producator
    app.post('/producators' , producatorCntrl.addProducator);
    //delete producator by id
    app.delete('/producators/:id' , producatorCntrl.deleteProducator);
    //update producator by id
    app.put('/producators/:id' , producatorCntrl.updateProducator);

    //GET PRPODUCT BY PRODUCATOR
    app.get('products/by/:name' , productsCntrl.productsByProducator);

};
