//requiries
var bodyParser = require('body-parser');
var express = require('express');

//init app
var app = express();
app.use(bodyParser.json());


//Separate front-end
app.use(express.static(__dirname + '/../public'));
//on wich port work server
var PORT = 3030;

//require routes
require('./routes.js').routes(app);

//start server
var server = app.listen(PORT , function listen () {
    console.log('server work on ' , PORT);
    console.log('http://127.0.0.1:' , PORT);
});
